(ns trail.active
  (:require [clojure.set :as set]
            [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [taoensso.timbre :as timbre
             :only (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :only (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.logic :as logic])
  (:use midje.sweet
        plumbing.core)
  (:gen-class))

(timbre/set-level! :trace)
(timbre/set-config! [:timestamp-pattern] "HH:mm:ss")
(timbre/set-config! [:prefix-fn]
                    (fn [{:keys [level timestamp hostname ns]}]
                      (str timestamp " " (-> level name string/upper-case)
                           " [" ns "]")))

;;; # Finding active trails
;;;
;;; Algorithm as described p.75 of Probabilistic Graphical Models,
;;; Daphne Koller and Nir Friedman.

;;; Graph representation 1
;;; Set of vertices and set of edges

(def G1 {:vertices #{1,2,3,4,5,6}
         :edges #{[1 2] [2 3] [6 5] [6 2] [5 4] [4 2]}})

;;; Graph representation 2
;;; Map from vertices to sets of their children.

(def G2 {1 #{2}
         2 #{3}
         3 #{}
         4 #{2}
         5 #{4}
         6 #{5 2}})

(defn sets-rep->nbd-map-rep
  [g]
  (merge (into {} (for [v (:vertices g)] [v #{}]))
         (map-vals (comp set (partial map second)) (group-by first (:edges g)))))

(fact
  (sets-rep->nbd-map-rep G1) => G2)

;;; Enrich the structure to also have the set of parents

(defn parent-nodes
  [g n]
  (set (filter (fn [p] (contains? (g p) n)) (keys g))))

(fact
  (parent-nodes G2 3) => #{2}
  (parent-nodes G2 2) => #{1 4 6})

(defn enrich-with-parents
  [nbd-map]
  (let [parent-map   (into {} (for [n  (keys nbd-map)] [n (parent-nodes nbd-map n)]))]
    (merge-with (fn [c p] {:children c :parents p}) nbd-map parent-map)))

(fact
  (enrich-with-parents G2)
  =>
  {1 {:children #{2}, :parents #{}},
   2 {:children #{3}, :parents #{1 4 6}},
   3 {:children #{}, :parents #{2}},
   4 {:children #{2}, :parents #{5}},
   5 {:children #{4}, :parents #{6}},
   6 {:children #{2 5}, :parents #{}}})

(def G3 (enrich-with-parents G2))

;;; Compute the set of ancestors of Z in G
;;; G is the graph: we presume this graph to be in nbd representation enriched
;;;    with parents
;;; Z is the set of which we compute the ancestors
;;; L is the set of nodes still to visit
;;; A is the currently computed set of ancestors

(defn ancestors
  ([G Z]
     (ancestors G Z #{}))

  ([G [y & more :as  L] A]
     (if (empty? L)
       A
       (if (not (contains? A y))
         (ancestors G (reduce conj more (:parents (G y))) (conj A y))
         (ancestors G more A)))))

(fact
  (ancestors G3 [1]) => #{1}
  (ancestors G3 [2]) => #{2 1 4 6 5}
  (ancestors G3 [3]) => #{3 2 1 4 6 5}
  (ancestors G3 [1 2 3]) => #{3 2 1 4 6 5}
  (ancestors G3 [6]) => #{6})
