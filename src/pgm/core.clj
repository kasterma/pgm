(ns pgm.core
  "Core for Probabilistic Graphical Models.

   All contents likely will be moved to other files when doneish."
  (:require [clojure.set :as set]
            [clojure.math.combinatorics :as combinat]
            [clojure.tools.cli :as cli]
            [taoensso.timbre :as timbre
             :refer (trace debug info warn error fatal spy)]
            [taoensso.timbre.profiling :as profiling :refer (p profile)]
            [clojure.core.reducers :as r]
            [clojure.core.logic :as logic])
  (:gen-class))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Context
;;;
;;; A context contains all the basic objects for a particular problem.

(defprotocol PContext
  (add-variable [_ name var])
  (del-variable [_ name])
  (add-factor [_ name factor])
  (del-factor [_ name]))

(defrecord Context [variable-map factor-map]
  PContext
  (add-variable
    [_ name var]
    (Context. (assoc variable-map name var)
              factor-map))

  (del-variable
    [_ name]
    (Context. (dissoc variable-map name) factor-map))

  (add-factor
    [_ name factor]
    (Context. variable-map
              (assoc factor-map name factor)))

  (del-factor
    [_ name]
    (Context. variable-map
              (dissoc factor-map name))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Variable
;;;
;;; First we give a representation for variables and their scopes.  A
;;; variable has a name and a finite set of values it can have.  The
;;; set of values it can have is represented by a vector so that it has
;;; an enumeration of the values included.

(defrecord Variable [name values])

(defn make-variable
  [name & values]
  {:pre [(pos? (count values))]}
  (Variable. name (vec values)))

(defn nonum-keyword [x] (if (number? x) x (keyword x)))

(defmacro defvariable [name & values]
  `(def ~name (apply make-variable
                     (str '~name)
                     (map nonum-keyword '(~@values)))))

(defn variable-scope-size
  "Get the size of the scope of a variable"
  [var]
  (count (:values var)))

(defn variable-get-index [var val]
  (.indexOf (:values var) val))

(defn variable-get-name [var idx]
  ((:values var) idx))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Factor
;;;
;;; A factor consists of:
;;;
;;; 1. A list of variables (this is the scope), and
;;; 2. the values for the different assignments in this scope.
;;;
;;; See assignment-in-scope to see which values corresponds to which
;;; assignment.

(defrecord Factor [name variables values])

(defn make-factor
  "Create a factor.  In case no values are given, the factor is constant 0."
  ([name vars]
     (Factor. name
              vars
              (vec (take (apply * (map variable-scope-size vars)) (repeat 0)))))

  ([name vars vals]
     {:pre [(= (apply * (map variable-scope-size vars)) (count vals))]}
     (Factor. name vars vals)))

(defn deffactor_h
  "helper function for the deffactor macro.  Given a name and
   arguments, split the arguments into variables and values, and than
   call the appropriate make-factor."
  [name & args]
  (let [grouped     (group-by (partial instance? Variable) args)]
    (if (zero? (count (grouped false)))
      (make-factor name (grouped true))
      (make-factor name (grouped true) (grouped false)))))

(defmacro deffactor [name & args]
  `(def ~name (deffactor_h (str '~name) ~@args)))

;;; ## Getting assignmentes and indices related to a factor

(defn assignment-in-scope
  "Given a scope and an index, give the index-th assignment in that
   scope"
  [[cur & rest-scope :as scope] index]
  {:pre [(not (neg? index))]}
  (let [curval     (variable-get-name cur (rem index (variable-scope-size cur)))
        rem-index  (quot index (variable-scope-size cur))]
    (if (empty? rest-scope)
      (do
        (assert (zero? rem-index))   ; otherwise the index was too large
        [curval])
      (apply conj [curval] (assignment-in-scope rest-scope rem-index)))))

(defn get-assignment
  "Given a factor and index, give the index-th assignment for that
   factor"
  [{scope :variables} index]
  (assignment-in-scope scope index))

(defn index-in-scope
  "Given an assignment and scope, give the index of the assign in that
   scope"
  ([[variable & rest-scope] [cur-assign & rest-assign]]
     (+ (variable-get-index variable cur-assign)
        (index-in-scope rest-scope
                        rest-assign
                        (variable-scope-size variable))))

  ([[variable & rest-scope] [cur-assign & rest-assign] sum]
     (if [empty? rest-assign]
       (* sum (variable-get-index variable cur-assign))
       (+ (* sum cur-assign)
          (index-in-scope rest-scope
                          rest-assign
                          (+ sum (variable-scope-size variable)))))))

(defn get-index
  "Given a factor and assignment, give the index of the assign in that
   factor"
  [{scope :variables} assignment]
  (index-in-scope scope assignment))

(defn get-factor-value
  [factor assignment]
  (let [assign-idx   (get-index factor assignment)]
    ((:values factor) assign-idx)))

(defn set-factor-value
  "Given a factor, an assignment suitable for its scope and a value,
   set that value to be the value of the factor on that assignment.

   Note: the name of the factor does not change."
  [{:keys [name variables values] :as factor} assignment new-value]
  (Factor. name variables (assoc values
                            (get-index factor assignment)
                            new-value)))

(defn factor-scope-size [{:keys [variables]}]
  (apply * (map variable-scope-size variables)))

(defn factor-table
  "Get a table representation of the given factor"
  [factor]
  (for [index  (range 0 (factor-scope-size factor))]
     [(get-assignment factor index)
      (nth (:values factor) index)]))

(defn factor-header
  "Get the variable names for in the header of a factor table"
  [{:keys [variables]}]
  (map :name variables))

(defn print-factor-table
  "Print the table representation of the given factor"
  [factor]
  (let [indices (range 0 (factor-scope-size factor))]
    (loop [index (first indices)
           rem   (rest indices)]
      (println (get-assignment factor index) "value" (nth (:values factor) index))
      (if (not (empty? rem))
        (recur (first rem) (rest rem))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Table representations of factors
;;;
;;; To make a more readable version for input of a factor we introduce
;;; deftable and table->factor.
;;;
;;; (deftable table1  | var1 | var2 | values |
;;;                   | a    | a    | 1      |
;;;                   | a    | b    | 2      |
;;;                   | c    | a    | 3      |
;;;                   | c    | b    | 4      |)
;;;
;;; After this
;;;
;;; (table->factor table1)
;;;
;;; returns the factor that is represented by the table.
;;;
;;; Two important notes on the table:
;;; 1. the spaces around | are important, and
;;; 2. the keyword values is required in the last column.
;;;
;;; Just declaring the symbol so we can use it in table defs
;;; Note: spaces around it are essential

(defn parse-table
  "Given a vector that contains a table (such as obtained from deftable),
   create a clojure table from it.  The rows and columns are the same as
   in the original table."
  [table]
  (->> (partition-by #(= :| %) table)     ; split by |
       (remove #(= (list :|) %))          ; remove (|)
       (partition-by #(= (list :| :|) %))  ; split by (| |)
       (remove #(= (list (list :| :|)) %)) ; remove ((| |))
       (map (partial apply concat))))          ; join rows

(defn transpose
  [m]
  (apply map list m))

(defn table->factor
  "Given a table, create the factor as indicated by the table"
  [table name]
  (let [ctable    (parse-table table)
        variables (->> (transpose ctable)
                       drop-last
                       (map (fn [[name & vals]]
                              (apply make-variable name (distinct vals)))))
        factor    (make-factor name variables)
        values    (rest ctable)]
    (reduce (fn [f assign-val] (set-factor-value f (drop-last assign-val) (last assign-val)))
            factor values)))

(defmacro deftable [name & table]
  `(def ~name (map nonum-keyword '(~@table))))

(defn var-intersect
  "Compute the intersection of l1 and l2"
  [l1 l2]
  (keep #(some #{%} l2) l1))

(defn indices
  "Get the indices in l1 of the elements remaining after (operation l1 l2)
  The assumption is that a subset of l1 is obtained."
  [l1 l2 operation]
  (let [subset (operation l1 l2)
        indices (map #(.indexOf l1 %) subset)]
    indices))

(defn var-index-intersect
  "Get the indices in l1 of the elements that are common to l1 and l2"
  [l1 l2]
  (indices l1 l2 var-intersect))

(defn var-complement
  "Compute l1 minus l2]"
  [l1 l2]
  (filter #(= -1 (.indexOf l2 %)) l1))

(defn var-index-complement
  "Compute the indices in l1 of the elemnts not in l2"
  [l1 l2]
  (indices l1 l2 var-complement))

(defn assignment-consistent
  "Check if the assignment with assign-index in factor is conssistent with
   the given evidence"
  [factor assign-index evidence]
  (let [assignment-map   (zipmap (:variables factor)
                                 (get-assignment factor assign-index))]
    (= (select-keys assignment-map (keys evidence))
       (select-keys evidence (keys assignment-map)))))

;; (defn restrict-assignment [assignment keep-indices]
;;   "Keed only the parts of the assignment at indices keep-indices"
;;   (map #(nth assignment %) keep-indices))

;; (defn const-vect [val length]
;;   (if (= 0 length)
;;     []
;;     (cons val (const-vect val (- length 1)))))

;; (defn update-vector [[t & tt] [a & as] [v & va] mm]
;;   (if (not (nil? tt))
;;     (let [nn (update-vector tt as va mm)]
;;       (if t
;;         (assoc nn a (+ v (nn a)))
;;         nn))
;;     (if t
;;       (assoc mm a (+ v (mm a)))
;;       mm)))

;; (defn reduce-factor [{:keys [vars scope values] :as factor} evidence]
;;   "Reduce the factor to take the given evidence into account"
;;   (let [keep-indices    (var-index-complement vars (keys evidence))
;;         new-vars        (map #(nth vars %) keep-indices)
;;         new-scope       (map #(nth scope %) keep-indices)
;;         assign-inds     (range 0 (count values))
;;         assign-consis   (map #(assignment-consistent factor % evidence) assign-inds)
;;         assignments     (map #(assignment-in-scope scope %) assign-inds)
;;         res-assignments (map #(restrict-assignment % keep-indices) assignments)
;;         res-assign-inds (map #(index-in-scope new-scope %) res-assignments)
;;         new-values      (update-vector assign-consis
;;                                        res-assign-inds
;;                                        values
;;                                        (vec (const-vect 0 (apply * new-scope))))]
;;     (make-factor new-vars new-scope new-values)))



;; (reduce-factor f1 {1 0})

;; ;; the following helper functions for the construction of factor-product
;; ;; there is a fixed order in how the variables are used
;; ;; This order is: first the variables from the first factor, then the
;; ;; variables from the second factor that are not also in the first factor
;; ;; in the order they appear in the second factor.

;; (defn vector-union [vect1 vect2]
;;   "Compute the union of the two vectors using the standard order"
;;   (let [add-vect-indices (var-index-complement vect2 vect1)]
;;     (if (empty? add-vect-indices)
;;       vect1
;;       (apply conj vect1 (map #(nth vect2 %) add-vect-indices)))))

;; (defn var-union [{vars1 :vars} {vars2 :vars}]
;;   "Compute the union of the variable sets in factor1 and factor2"
;;   (vector-union vars1 vars2))

;; (defn scope-union [{vars1 :vars, scope1 :scope}
;;                    {vars2 :vars, scope2 :scope}]
;;   "Compute the uinon of the scopes in the two argument factors"
;;   (let [add-vect-indices (var-index-complement vars2 vars1)]
;;     (apply conj scope1 (map #(nth scope2 %) add-vect-indices))))

;; (var-union f1 f1)
;; (var-union f2 f1)
;; (var-union f1 f2)

;; (defn assignments-consistent [{vars1 :vars,
;;                                values1 :values :as factor1}
;;                               index1
;;                               {vars2 :vars,
;;                                values2 :values :as factor2}
;;                               index2]
;;   "Returns a pair [x y] with x != nil iff the assign are consistent,
;;    then y is the index in the product factor"
;;   (let [var2-indices   (var-index-complement vars2 vars1)
;;         scope-union    (scope-union factor1 factor2)
;;         assignment1    (get-assignment factor1 index1)
;;         assignment2    (get-assignment factor2 index2)
;;         assignment     (apply conj assignment1 (map #(nth assignment2 %) var2-indices))
;;         index          (index-in-scope scope-union assignment)
;;         value          (* (nth values1 index1) (nth values2 index2))]
;;                                         ; Now we have both assignments and the info
;;                                         ; to return if consistent.  Run through
;;                                         ; assignment1 checking for consistency.
;;                                         ; Need to check only when cur-var is in vars2.
;;     (loop [cur-var         (first vars1)
;;            cur-value       (first assignment1)
;;            rem-vars        (rest vars1)
;;            rem-assignment  (rest assignment1)]
;;       (let [cur-index2  (.indexOf vars2 cur-var)]
;;         (if (= -1 cur-index2)           ; If true than cur-var is not in vars2
;;           (if (not (empty? rem-assignment))
;;             (recur (first rem-vars)
;;                    (first rem-assignment)
;;                    (rest rem-vars)
;;                    (rest rem-assignment))
;;             [1 index value])
;;           (if (= (nth assignment2 cur-index2) cur-value)
;;             (recur (first rem-vars)
;;                    (first rem-assignment)
;;                    (rest rem-vars)
;;                    (rest rem-assignment))
;;             [nil index value]))))))

;; (defn factor-product [{vars1 :vars, scope1 :scope, values1 :values :as factor1}
;;                       {vars2 :vars, scope2 :scope, values2 :values :as factor2}]
;;   "Compute the product of the factors factor1 and factor2"
;;   (let [new-vars             (var-union factor1 factor2)
;;         new-scope            (scope-union factor1 factor2)
;;         consis-ass-triples   (for [idx1 (range 0 (apply * scope1))
;;                                    idx2 (range 0 (apply * scope2))]
;;                                (assignments-consistent factor1 idx1
;;                                                        factor2 idx2))
;;                                         ; consis-ass-triples contains triples
;;                                         ; [1/nil, assign-index, related value]
;;         assign-consistent    (map first consis-ass-triples)
;;         assign-indices       (map second consis-ass-triples)
;;         assign-values        (map #(nth % 2) consis-ass-triples)
;;         new-values           (update-vector assign-consistent
;;                                             assign-indices
;;                                             assign-values
;;                                             (vec (const-vect 0 (apply * new-scope))))]
;;     (make-factor new-vars new-scope new-values)))


;; (defn normalize-factor [{:keys [vars scope values] :as factor}]
;;   "Normlize the factor so the values sum to 1"
;;   (let [total         (apply + values)
;;         norm-values   (map #(/ % total) values)]
;;     (make-factor vars scope norm-values)))

;; (defn assignment-value-marginalize [{scope :scope, values :values}
;;                                     index
;;                                     new-vars-indices]
;;   "Marginalize the index-th assignment to new-vars and give the value there"
;;   (let [assignment       (assignment-in-scope scope index)
;;         marg-assignment  (restrict-assignment assignment new-vars-indices)
;;         marg-index       (index-in-scope scope marg-assignment)
;;         marg-value       (nth values index)]
;;     `(~marg-index ~marg-value)))

;; (defn factor-marginalize [{:keys [vars scope values] :as factor}
;;                          sumout-vars]
;;   "Marginalize the factor w.r.t. sumout-vars"
;;   (let [new-vars-indices    (var-index-complement vars sumout-vars)
;;         new-vars            (map #(vars %) new-vars-indices)
;;         new-scope           (map #(scope %) new-vars-indices)
;;         old-max-index       (apply * scope)
;;         marg-assign-pairs   (map #(assignment-value-marginalize factor
;;                                                                 %
;;                                                                 new-vars-indices)
;;                                  (range 0 old-max-index))
;;         assign-consistent   (const-vect 1 old-max-index)
;;         assign-indices      (map first marg-assign-pairs)
;;         assign-values       (map second marg-assign-pairs)
;;         new-values          (update-vector assign-consistent
;;                                            assign-indices
;;                                            assign-values
;;                                            (vec (const-vect 0 (apply * new-scope))))]
;;     (make-factor new-vars new-scope new-values)))
