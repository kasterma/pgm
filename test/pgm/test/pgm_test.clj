(ns pgm.test.pgm-test
  (:use pgm.core
        midje.sweet))

(defvariable var1 1 2 3)
(defvariable var2 :a :b)
(defvariable var3 a b)


(facts "variable scope size is computed correctly"
  (variable-scope-size var1) => 3
  (variable-scope-size var2) => 2
  (make-variable :var2) => (throws AssertionError))


(facts "getting index and name of variable"
  (variable-get-index var1 1) => 0
  (variable-get-index var2 :b) => 1
  (variable-get-name var2 1) => :b)


(deffactor f1 var1 var2 1 2 3 4 5 6)

(print-factor-table f1)

(deffactor f2 var1 2 2 2)

(deffactor f3 var1 var2)

(facts "checking factor-table (and with that deffactor)"
  (factor-table f1) => [[[1 :a] 1] [[2 :a] 2] [[3 :a] 3] [[1 :b] 4] [[2 :b] 5] [[3 :b] 6]]
  (factor-table f2) => [[[1] 2] [[2] 2] [[3] 2]]
  (factor-table f3) => [[[1 :a] 0] [[2 :a] 0] [[3 :a] 0] [[1 :b] 0] [[2 :b] 0] [[3 :b] 0]]

  (factor-header f1) => ["var1" "var2"]
  (factor-header f2) => ["var1"]
  (factor-header f3) => ["var1" "var2"])

(facts "deffactor checks that the correct no of values is given"
  (deffactor ff var1 1) => (throws AssertionError))


(facts "get-assignment gets the right assignment"
  (let [scope   [var1 var2]]
    (tabular (assignment-in-scope scope ?idx) => ?assignment
             ?idx ?assignment
             0    [1 :a]
             1    [2 :a]
             2    [3 :a]
             3    [1 :b]))
  (tabular (get-assignment f1 ?idx) => ?assignment
           ?idx ?assignment
           -1   (throws AssertionError)
           0    [1 :a]
           1    [2 :a]
           2    [3 :a]
           3    [1 :b]
           4    [2 :b]
           5    [3 :b]
           6    (throws AssertionError)))


(facts "get-index gets the right index"
  (let  [scope   [var1 var2]]
    (tabular (index-in-scope scope ?assignment) => ?idx
             ?idx ?assignment
             0    [1 :a]
             1    [2 :a]
             2    [3 :a]
             3    [1 :b]))
  (tabular (get-index f1 ?assignment) => ?idx
           ?idx ?assignment
           0    [1 :a]
           1    [2 :a]
           2    [3 :a]
           3    [1 :b]
           4    [2 :b]
           5    [3 :b]))


(facts "get/set factor value work as intended"
  (get-factor-value f1 [2 :b]) => 5
  (get-factor-value (set-factor-value f1 [2 :b] 481) [2 :b]) => 481)


(deftable table1  | :var1 | :var2 | :values |
                  | :a    | :a    | 1       |
                  | :a    | :b    | 2       |
                  | :c    | :a    | 3       |)

(deftable table1b  | var1 | var2 | values |
                   | a    | a    | 1       |
                   | a    | b    | 2       |
                   | c    | a    | 3       |)

(facts "no need to use keywords"
  (= table1 table1b) => true)

(deftable table2  | :var3 | :values |
                  | :a    | 1       |
                  | :b    | 2       |
                  | :c    | 3       |)

(facts "parsing tables gives clojure tables"
  (parse-table table1) => '((:var1 :var2 :values) (:a :a 1) (:a :b 2) (:c :a 3))
  (parse-table table2) => '((:var3 :values) (:a 1) (:b 2) (:c 3)))

(facts "transpose transposes"
  (transpose '((1 2) (3 4))) => '((1 3) (2 4)))

(facts "check table->factor"
  (let [ft1 (table->factor table1 "testtable")]
    (get-factor-value ft1 [:a :a]) => 1
    (get-factor-value ft1 [:a :b]) => 2
    (get-factor-value ft1 [:c :a]) => 3
    (get-factor-value ft1 [:c :b]) => 0))

(facts "check var-intersect"
  (var-intersect [var1 var2] [var2 var3]) => #(= ["var2"] (map :name %))
  (var-intersect [var1 var2] [var2 var1]) => #(= ["var1" "var2"] (map :name %)))

(facts "check indices fn"
  (indices [1 2 3 4] [3 4] (fn [x y] (clojure.set/difference (set x) (set y)))) => [0 1]
  (indices [1 2 3 4] [3 4] (fn [x y] (reverse x))) => [3 2 1 0])

(facts "var-complement"
  (var-complement [var1 var2] [var2]) => #(= ["var1"] (map :name %)))

(facts "assignment-consistent"
  (get-assignment f1 2) => [3 :a]
  (assignment-consistent f1 2 {var2 :a}) => true
  (assignment-consistent f1 2 {var1 3}) => true
  (assignment-consistent f1 2 {var1 3 var2 :a}) => true
  (assignment-consistent f1 2 {var1 3 var2 :b}) => false
  (assignment-consistent f1 2 {var2 :b}) => false)
