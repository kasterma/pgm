# pgm

Basic implementations of probabilistic graphical models.

![Travis build status](https://api.travis-ci.org/kasterma/pgm.png)

## License

Copyright (C) 2012 Bart Kastermans

Distributed under the Eclipse Public License, the same as Clojure.
