(defproject pgm "1.0.0-SNAPSHOT"
  :description "implementation of basic functions around probabilistic graphical models"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/data.json "0.2.3"]
                 [org.clojure/math.combinatorics "0.0.4"]
                 [org.clojure/tools.cli "0.2.4"]
                 [midje "1.5.1"]
                 [com.taoensso/timbre "2.6.3"]
                 [org.clojure/core.logic "0.8.4"]
                 [org.clojure/algo.monads "0.1.4"]
                 [prismatic/plumbing "0.1.1"]
                 [prismatic/schema "0.1.6"]]
  :jvm-opts ["-XX:MaxPermSize=128M"
             "-XX:+UseConcMarkSweepGC"
             "-Xms2g" "-Xmx2g" "-server"]
  ;; needed to use midje on travis
  :plugins [[lein-midje "3.0.0"]])
